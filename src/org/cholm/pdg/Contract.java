package org.cholm.pdg;

/**
 * Various constants used through out the application
 * 
 * @author cholm
 * 
 */
public final class Contract
{
    /**
     * Cannot instantise this class
     */
    private Contract()
    {}

    /** Particles table name */
    public static final String PARTICLES_TABLE     = "Particles";
    /** Anti-particles table name */
    public static final String ANTIPARTICLES_TABLE = "AntiParticles";
    /** Decays table name */
    public static final String DECAYS_TABLE        = "Decays";
    /** Meta table */
    public static final String META_TABLE          = "Meta";

    public static final String CODE_COLUMN         = "code";
    public static final String NAME_COLUMN         = "name";
    public static final String PRETTY_COLUMN       = "pretty_name";
    public static final String ANTI_COLUMN         = "has_anti";
    public static final String TYPE_COLUMN         = "type";
    public static final String CHARGE_COLUMN       = "charge";
    public static final String MASS_COLUMN         = "mass";
    public static final String WIDTH_COLUMN        = "width";
    public static final String ISO_COLUMN          = "iso_spin";
    public static final String ISO3_COLUMN         = "iso3_column";
    public static final String SPIN_COLUMN         = "spin";

    public static final String MOTHER_COLUMN       = "mother";
    public static final String RATIO_COLUMN        = "branching_ratio";
    public static final String DAUGHTER_COLUMN     = "daughter";
    public static final String NPROD_COLUMN        = "n";

}
/*
 * EOF
 */
