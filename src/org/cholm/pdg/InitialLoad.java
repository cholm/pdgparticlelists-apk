/*
 * Android application to show particle properties
 * Copyright © 2010 Christian Holm Christensen <cholm@nbi.dk>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 
 */
package org.cholm.pdg;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;

/**
 * @author cholm
 * 
 */
public class InitialLoad extends Activity
{

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Theme.instance().apply(this);
        setContentView(R.layout.initial_load);
        this.showDialog(0);
    }

    @Override
    public Dialog onCreateDialog(int which)
    {
        Log.d("Overview", "Not populated yet - ask user");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(Html.fromHtml(getString(R.string.ask_populate)))
                        .setCancelable(false)
                        .setPositiveButton("Yes",
                                           new DialogInterface.OnClickListener() {
                                               public void onClick(DialogInterface dialog,
                                                                   int id)
                                               {
                                                   setResult(Activity.RESULT_OK);
                                                   InitialLoad.this.finish();
                                               }
                                           })
                        .setNegativeButton("No",
                                           new DialogInterface.OnClickListener() {
                                               public void onClick(DialogInterface dialog,
                                                                   int id)
                                               {
                                                   dialog.cancel();
                                                   setResult(Activity.RESULT_CANCELED);
                                                   InitialLoad.this.finish();
                                               }
                                           });
        return builder.create();
    }

    @Override
    public void onBackPressed()
    {
        setResult(Activity.RESULT_CANCELED);
        InitialLoad.this.finish();
    }
}
