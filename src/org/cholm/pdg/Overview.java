/*
 * Android application to show particle properties
 * Copyright © 2010 Christian Holm Christensen <cholm@nbi.dk>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.cholm.pdg;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.BaseColumns;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

/**
 * The main activity. Shows all particles in a grid view
 * 
 * @author cholm
 * 
 */
public class Overview extends Activity
{
    private final String TAG             = Provider.BASE_TAG + ".Overview";

    /** Progress dialog id */
    private final int    PROGRESS_DIALOG = 0;
    /** Populate dialog id */
    private final int    POPULATE_DIALOG = 1;
    /** About dialog id */
    private final int    ABOUT_DIALOG    = 2;
    /** Filter dialog id */
    private final int    FILTER_DIALOG   = 3;
    /** State of initial load */
    private final int    POPULATING      = 1;
    /** State of initial load */
    private final int    LOADING         = 2;
    /** State of initial load */
    private final int    DONE            = 3;

    /**
     * Thread run to change state of progress meter and load data
     * 
     * @author cholm
     * 
     */
    private class RefreshThread extends Thread
    {
        private final String TAG = Overview.this.TAG + ".Refresh";

        /**
         * Constructor
         * 
         * @param handler
         *            Handler of messages
         */
        public RefreshThread(Handler handler)
        {
            mHandler = handler;
        }

        /**
         * Make a message
         * 
         * @param st
         *            State id.
         */
        public void makeMessage(int st)
        {
            mState = st;
            Message msg = mHandler.obtainMessage();
            Bundle bnd = new Bundle();
            bnd.putInt("state", mState);
            msg.setData(bnd);
            Log.d(TAG, "sending message " + msg);
            mHandler.sendMessage(msg);
        }

        /**
         * Run this thread
         */
        @Override
        public void run()
        {
            do {
                if (checkDb()) break;
                if (mState == 0) makeMessage(POPULATING);

                try {
                    Thread.sleep(1000);
                }
                catch (InterruptedException e) {
                    Log.e(TAG + ".ProgressThread", "Thread Interrupted");
                }
            } while (true);

            Log.d(TAG, "Sending LOADING message");
            makeMessage(LOADING);
            try {
                Thread.sleep(100);
            }
            catch (InterruptedException e) {
                Log.e(TAG + ".ProgressThread", "Thread Interrupted");
            }
            Log.d(TAG, "Asking overview to update");
            Overview.this.update();

            Log.d(TAG, "Sending DONE message");
            makeMessage(DONE);
        }

        /** Handler */
        private Handler mHandler = null;
        /** State */
        private int     mState   = 0;
    }

    /**
     * Member function to generate a simple cursor adapter
     * 
     * @param c
     *            Cursor to use or null
     * 
     * @return true
     */
    private SimpleCursorAdapter makeAdapter(Cursor c)
    {
        String[] from = new String[] { Contract.PRETTY_COLUMN };
        int[] to = new int[] { R.id.entry };
        SimpleCursorAdapter ret =
                                  new SimpleCursorAdapter(
                                                          this,
                                                          R.layout.overview_entry,
                                                          c, from, to);
        ret.setViewBinder(new SimpleCursorAdapter.ViewBinder() {

            public boolean setViewValue(View view,
                                        Cursor cursor,
                                        int columnIndex)
            {
                TextView tv = (TextView) view;
                String pretty = cursor.getString(columnIndex);
                Spanned ret = Html.fromHtml(pretty);
                Log.d(TAG, "Translating " + pretty + " -> " + ret);
                tv.setText(ret);
                return true;
            }
        });
        return ret;
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        Log.d(TAG, "Creating overview");
        super.onCreate(savedInstanceState);
        Theme.instance().apply(this);
        setContentView(R.layout.main);

        mAdapter = makeAdapter(null);
        // mAdapter = new OverviewAdapter(this);

        mGrid = (GridView) findViewById(R.id.gridview);
        mGrid.setAdapter(mAdapter);
        mGrid.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent,
                                    View v,
                                    int position,
                                    long id)
            {
                showParticle(id);
            }
        });
        this.registerForContextMenu(mGrid);

        if (!checkDb()) {
            mStartMsg = R.string.load_populating;
            Intent intent = new Intent(this, InitialLoad.class);
            this.startActivityForResult(intent, POPULATE_DIALOG);
        }
        else doIntent();
    }

    /**
     * Called when resuming the task
     */
    @Override
    protected void onResume()
    {
        Log.d(TAG, "Resuming overview");
        // showDialog(PROGRESS_DIALOG);
        // refresh(mSelection, mSelectArgs);
        // update();
        super.onResume();
    }

    /**
     * Called when we need to figure out what to do.
     * 
     */
    protected void doIntent()
    {
        Intent intent = getIntent();
        // Log.d(TAG, "Doing intent " + intent);
        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            // handles a click on a search suggestion; launches activity to show
            // word
            long id = Long.valueOf(intent.getData().getLastPathSegment());
            showParticle(id);
            finish();
        }
        else if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            // handles a search query
            String query = intent.getStringExtra(SearchManager.QUERY);
            // Log.d(TAG, "query=" + (query == null ? "NULL" : query));
            refresh(Contract.NAME_COLUMN + " LIKE '" + query + "%'", null);
        }
        else {
            mSelection = null;
            mSelectArgs = null;
            // update();
            refresh(null, null);
        }
    }

    /**
     * Called when configuration (i.e., screen orientation) is changed.
     */
    @Override
    public void onConfigurationChanged(Configuration cfg)
    {
        super.onConfigurationChanged(cfg);
    }

    /**
     * Called when a child activity finished
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == POPULATE_DIALOG) {
            if (resultCode == RESULT_CANCELED) this.finish();

            populateDb();
            doIntent();
        }
    }

    /**
     * Called when creating the options menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.overview_menu, menu);
        return true;
    }

    /**
     * Called when an options menu item is selected
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {
        case R.id.search:
            onSearchRequested();
            return true;
        case R.id.quit:
            finish();
            return true;
        case R.id.filter:
            Log.d(TAG, "Should make filter activity");
            showDialog(FILTER_DIALOG);
            return true;
        case R.id.about:
            Log.d(TAG, "Should show about dialog");
            showDialog(ABOUT_DIALOG);
            return true;
        default:
            return false;
        }
    }

    /**
     * Create a context menu
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu,
                                    View v,
                                    ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
        long code = info.id;
        Uri u =
                Uri.withAppendedPath(Provider.PARTICLE_URI, Long.toString(code));
        Cursor c = managedQuery(u, null, null, null, null);
        boolean hasAnti = true;
        if (c == null || c.getCount() < 1 || !c.moveToFirst()) hasAnti = false;
        if (c.getInt(3) == 0) hasAnti = false;
        inflater.inflate(R.menu.context_menu, menu);
        menu.getItem(1).setEnabled(hasAnti);
    }

    /**
     * On context menu selection.
     */
    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        AdapterContextMenuInfo info =
                                      (AdapterContextMenuInfo) item
                                                      .getMenuInfo();
        long code = info.id;
        switch (item.getItemId()) {
        case R.id.show_particle:
            showParticle(code);
            return true;
        case R.id.show_antiparticle:
            showParticle(-code);
            return true;
        default:
            return super.onContextItemSelected(item);
        }
    }

    /**
     * Check if the database has been populated
     * 
     * @return
     */
    protected boolean checkDb()
    {
        Cursor cursor =
                        managedQuery(Provider.DB_URI, null, "exists", null,
                                     null);
        boolean ret = true;
        if (cursor == null) return false;
        if (cursor.getCount() <= 0 || !cursor.moveToFirst()) {
            ret = false;
        }
        else {
            ret = cursor.getInt(0) > 0;
        }
        this.stopManagingCursor(cursor);
        if (!cursor.isClosed()) cursor.close();
        return ret;
    }

    /**
     * Ask the provider to populate the database
     */
    protected void populateDb()
    {
        Log.d(TAG, "Will populate DB");
        Cursor cursor =
                        managedQuery(Provider.DB_URI, null, "populate", null,
                                     null);
        this.stopManagingCursor(cursor);
        if (!cursor.isClosed()) cursor.close();
    }

    /**
     * Show a particle
     * 
     * @param code
     *            Particle code
     */
    protected void showParticle(long code)
    {
        String arg = Integer.toString((int) code);
        Intent particleIntent = new Intent(this, ParticleView.class);
        particleIntent.putExtra(Contract.CODE_COLUMN, code);
        particleIntent.setData(Uri.withAppendedPath(Provider.PARTICLE_URI, arg));
        startActivity(particleIntent);
    }

    /**
     * Update our query using the stored selections
     * 
     * This is called in the refresh thread
     */
    protected void update()
    {

        Log.d(TAG, "Updating");

        String[] columns =
                           new String[] { Contract.PRETTY_COLUMN,
                                           BaseColumns._ID };
        mCursor =
                  managedQuery(Provider.PARTICLES_URI, columns, mSelection,
                               mSelectArgs, null);

        Log.d(TAG, "Got a total of " + mCursor.getCount()
                   + " entries from query " + mSelection);
    }

    /**
     * Refresh the view.
     * 
     * @param selection
     *            Selection
     * @param selectArgs
     *            Selection arguments
     */
    protected void refresh(String selection, String[] selectArgs)
    {
        mSelection = selection;
        mSelectArgs = selectArgs;
        showDialog(PROGRESS_DIALOG);
    }

    /**
     * Called when a dialog is to be shown
     */
    @Override
    protected void onPrepareDialog(int id, Dialog dialog)
    {
        switch (id) {
        case PROGRESS_DIALOG:
            RefreshThread thread = new RefreshThread(mHandler);
            thread.start();
            break;
        default:
            break;
        }
    }

    /**
     * Called when creating dialogs
     */
    @Override
    protected Dialog onCreateDialog(int id)
    {
        switch (id) {
        case PROGRESS_DIALOG:
            mLoadDialog = new ProgressDialog(this);
            mLoadDialog.setMessage(getResources().getText(this.mStartMsg));
            return mLoadDialog;
        case ABOUT_DIALOG:
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(Html.fromHtml(getString(R.string.about,
                                                       "&copy;",
                                                       getString(R.string.anti_start_tag),
                                                       getString(R.string.anti_end_tag),
                                                       getString(R.string.sparticle_start_tag),
                                                       getString(R.string.sparticle_end_tag))))
                            .setCancelable(true).setIcon(R.drawable.icon)
                            .setPositiveButton("OK", null).setTitle("About");

            return builder.create();
        case FILTER_DIALOG:
            // Context context = getApplicationContext();
            mFilter = new Dialog(this);

            mFilter.setContentView(R.layout.filter);
            mFilter.setTitle(R.string.filter_title);

            Button apply = (Button) mFilter.findViewById(R.id.filter_apply);
            Button cancel = (Button) mFilter.findViewById(R.id.filter_cancel);

            cancel.setOnClickListener(new OnClickListener() {
                /**
                 * Handle clicks
                 */
                public void onClick(View v)
                {
                    mFilter.dismiss();
                }
            });
            apply.setOnClickListener(new OnClickListener() {
                /**
                 * Check if an option is enabled
                 * 
                 * @param id
                 *            Id of UI element
                 * @return State of element
                 */
                public boolean checkOpt(int id)
                {
                    return ((CheckBox) mFilter.findViewById(id)).isChecked();
                }

                /**
                 * Append an AND selection condition
                 * 
                 * @param sel
                 *            Current selection
                 * @param what
                 *            What to append
                 * @return New selection string
                 */
                public String appendAnd(String sel, String what)
                {
                    String r = sel;
                    if (r.length() > 0) r += " AND ";
                    r += what;
                    return r;
                }

                /**
                 * Append an OR selection condition
                 * 
                 * @param sel
                 *            Current selection
                 * @param what
                 *            What to append
                 * @return New selection string
                 */
                public String appendOr(String sel, String what)
                {
                    String r = sel;
                    if (r.length() > 0) r += " OR ";
                    r += what;
                    return r;
                }

                /**
                 * Handle click
                 */
                public void onClick(View v)
                {
                    boolean charge = checkOpt(R.id.filter_charged);
                    boolean stable = checkOpt(R.id.filter_stable);
                    boolean quark = checkOpt(R.id.filter_quark);
                    boolean lepton = checkOpt(R.id.filter_lepton);
                    boolean guage = checkOpt(R.id.filter_guage);
                    boolean meson = checkOpt(R.id.filter_meson);
                    boolean baryon = checkOpt(R.id.filter_baryon);
                    boolean cmeson = checkOpt(R.id.filter_cmeson);
                    boolean cbaryon = checkOpt(R.id.filter_cbaryon);
                    boolean bmeson = checkOpt(R.id.filter_bmeson);
                    boolean bbaryon = checkOpt(R.id.filter_bbaryon);
                    boolean susy = checkOpt(R.id.filter_sparticle);
                    boolean gen = checkOpt(R.id.filter_generator);
                    boolean excited = checkOpt(R.id.filter_excited);
                    boolean other = checkOpt(R.id.filter_unknown);

                    String q = "";
                    String t = "";
                    if (charge) q =
                                    appendAnd(q, Contract.CHARGE_COLUMN
                                                 + " != 0");
                    if (stable) q =
                                    appendAnd(q, Contract.WIDTH_COLUMN
                                                 + " < 1e-10");
                    if (quark) t =
                                   appendOr(t, Contract.TYPE_COLUMN
                                               + " LIKE 'Quark'");
                    if (lepton) t =
                                    appendOr(t, Contract.TYPE_COLUMN
                                                + " LIKE 'Lepton'");
                    if (guage) t =
                                   appendOr(t, Contract.TYPE_COLUMN
                                               + " LIKE 'GuageBoson'");
                    if (meson) t =
                                   appendOr(t, Contract.TYPE_COLUMN
                                               + " LIKE 'Meson'");
                    if (baryon) t =
                                    appendOr(t, Contract.TYPE_COLUMN
                                                + " LIKE 'Baryon'");
                    if (cmeson) t =
                                    appendOr(t, Contract.TYPE_COLUMN
                                                + " LIKE 'CharmedMeson'");
                    if (cbaryon) t =
                                     appendOr(t, Contract.TYPE_COLUMN
                                                 + " LIKE 'CharmedBaryon'");
                    if (bmeson) t =
                                    appendOr(t, Contract.TYPE_COLUMN
                                                + " LIKE 'B-Meson'");
                    if (bbaryon) t =
                                     appendOr(t, Contract.TYPE_COLUMN
                                                 + " LIKE 'B-Baryon'");
                    if (susy) t =
                                  appendOr(t, Contract.TYPE_COLUMN
                                              + " LIKE 'Sparticle'");
                    if (gen) t =
                                 appendOr(t, Contract.TYPE_COLUMN
                                             + " LIKE 'Generatork'");
                    if (excited) t =
                                     appendOr(t, Contract.TYPE_COLUMN
                                                 + " LIKE 'Excited'");
                    if (other) t =
                                   appendOr(t, Contract.TYPE_COLUMN
                                               + " LIKE 'Other'");
                    if (t.length() > 0) q = appendAnd(q, " ( " + t + " ) ");
                    refresh(q, null);
                    mFilter.dismiss();
                }
            });
            return mFilter;
        default:
            return null;
        }
    }

    /**
     * Handle messages.
     * 
     * @author cholm
     * 
     */
    private final class MessageHandler extends Handler
    {
        private final String TAG = Overview.this.TAG + ".MessageHandler";

        /**
         * Handle the message.
         * 
         * This is called in the main thread
         */
        @Override
        public void handleMessage(Message msg)
        {
            int st = msg.getData().getInt("state");
            Log.d(TAG, "Got message " + msg + " " + st);
            switch (st) {
            case 0:
                mLoadDialog.setMessage(getResources()
                                .getText(R.string.load_please_wait));
                break;
            case POPULATING:
                mLoadDialog.setMessage(getResources()
                                .getText(R.string.load_populating));
                break;
            case LOADING:
                mLoadDialog.setMessage(getResources()
                                .getText(R.string.load_loading));
                break;
            case DONE:
                dismissDialog(PROGRESS_DIALOG);
                Log.d(TAG, "Got done signal, grid should update");
                Cursor old = mAdapter.getCursor();
                mAdapter.changeCursor(mCursor);
                if (old != null) {
                    stopManagingCursor(old);
                    if (!old.isClosed()) old.close();
                }
                // mAdapter.notifyDataSetChanged();
                break;
            default:
                Log.w(TAG, "Unknown message received");
                break;
            }
        }
    }

    /**
     * Define the Handler that receives messages from the thread
     * and update the progress
     */
    final Handler                 mHandler    = new MessageHandler();
    /** Adapter */
    protected SimpleCursorAdapter mAdapter    = null;
    /** Current database cursor */
    protected Cursor              mCursor     = null;
    /** Current selection */
    protected String              mSelection  = null;
    /** Selection arguments */
    protected String[]            mSelectArgs;
    /** Progress dialog */
    protected ProgressDialog      mLoadDialog = null;
    /** Master layout */
    protected GridView            mGrid       = null;
    /** Filter dialog */
    protected Dialog              mFilter     = null;
    /** Initial message in progress dialog */
    protected int                 mStartMsg   = R.string.load_please_wait;
}
/*
 * EOF
 */