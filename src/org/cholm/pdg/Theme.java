package org.cholm.pdg;

import java.lang.reflect.Field;

import android.content.Context;
import android.util.Log;

/**
 * Manager of theme identifier
 * 
 * @author cholm
 * 
 */
public class Theme
{
    /** Debug tag */
    private static final String TAG = Provider.BASE_TAG + ".Theme";

    /**
     * Singleton instance access
     * 
     * @return singleton
     */
    public static synchronized Theme instance()
    {

        if (mInstance == null) mInstance = new Theme();
        return mInstance;
    }

    /**
     * Apply theme to a context.
     * 
     * @param cx
     */
    public void apply(Context cx)
    {
        if (mThemeId < 0) return;
        cx.setTheme(mThemeId);
    }

    /**
     * Private constructor. Find the theme identifier
     */
    private Theme()
    {
        Log.d(TAG, "SDK is " + android.os.Build.VERSION.SDK_INT);
        try {
            Field f = android.R.style.class.getField("Theme_Holo");
            if (f == null) return;
            mThemeId = f.getInt(f);
            Log.d(TAG, "Found Theme_Holo in android.R.style: " + mThemeId);
        }
        catch (SecurityException e) {}
        catch (NoSuchFieldException e) {}
        catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            Log.d(TAG, "Illegal argument when getting int");
            e.printStackTrace();
        }
        catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            Log.d(TAG, "Illegal access modifier when getting int");
            e.printStackTrace();
        }
    }

    /** singleton instance */
    private static Theme mInstance = null;
    /** Theme identifier, or -1 if not found */
    protected int        mThemeId  = -1;
}
