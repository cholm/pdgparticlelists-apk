/*
 * Android application to show particle properties
 * Copyright © 2010 Christian Holm Christensen <cholm@nbi.dk>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 
 */
package org.cholm.pdg;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

/**
 * @author cholm
 *         A view of a particle
 */
public class ParticleView extends Activity
{
    /** Planks constant divided by 2 pi */
    static final double         HBAR         = 6.58211889e-25;                 // GeV
                                                                                // s
    /** Debug tag */
    public static final String  TAG          = Provider.BASE_TAG + ".Particle";
    private static final String ERROR_STRING = "<strong>Error</strong>";

    /**
     * Called when creating the activity
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Theme.instance().apply(this);
        setContentView(R.layout.particle_view);

        mInflater =
                    (LayoutInflater) this
                                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Intent intent = getIntent();
        mUri = intent.getData();

        int id = Integer.valueOf(mUri.getLastPathSegment());
        boolean asAnti = (id < 0);
        if (asAnti) {
            Uri.Builder b = mUri.buildUpon();
            b.path(Provider.PARTICLE_PART + "/" + (-id));
            mUri = b.build();
        }
        mCode = id;
        mAbsCode = Math.abs(id);
        fillFields(asAnti);
        fillDecays(asAnti);
    }

    /**
     * Called when options menu is made
     * 
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.particle_menu, menu);
        return true;
    }

    /**
     * Called when an option menu item is selected
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {
        case R.id.show_anti:
            showAntiParticle();
            break; // return true;
        case R.id.back:
            finish();
            break; // return true;
        default:
            break; // return false;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Fill in fields.
     * 
     * @param asAnti
     *            If true, then the result shown should be for the
     *            anti-particle. Note, that we will query the particle table for
     *            the
     *            properties, but will flip the charge etc. The anti-particle
     *            table
     *            is queried for the name of the particle. Note, that the
     *            anti-particle
     *            table will always be queried to get the name of the
     *            anti-particle
     *            (we need that in all cases).
     */
    protected void fillFields(boolean asAnti)
    {
        Cursor cursor = managedQuery(mUri, null, null, null, null);
        if (cursor == null) return;
        boolean ok = (cursor.getCount() >= 1 && cursor.moveToFirst());

        TextView name = (TextView) findViewById(R.id.name);
        TextView search_name = (TextView) findViewById(R.id.search_name);
        TextView code = (TextView) findViewById(R.id.code_value);
        TextView anti = (TextView) findViewById(R.id.anti_value);
        TextView mass = (TextView) findViewById(R.id.mass_value);
        TextView width = (TextView) findViewById(R.id.width_value);
        TextView charge = (TextView) findViewById(R.id.charge_value);
        TextView type = (TextView) findViewById(R.id.type_value);
        TextView life = (TextView) findViewById(R.id.lifetime_value);

        // mCode = (asAnti ? -1 : 1) * cursor.getInt(0);
        String vName = ok ? cursor.getString(1) : "?";
        String vPretty = ok ? cursor.getString(2) : "unknown";
        mHasAnti = ok ? cursor.getInt(3) != 0 : false;
        String vType = ok ? cursor.getString(4) : "?";
        int vQ = ok ? (asAnti ? -1 : 1) * cursor.getInt(5) : 0;
        double vMass = ok ? cursor.getDouble(6) : 0;
        double vWidth = ok ? cursor.getDouble(7) : 0;
        this.stopManagingCursor(cursor);
        if (!cursor.isClosed()) cursor.close();

        Spanned pname = Html.fromHtml(vPretty);
        Spanned apname = (mHasAnti ? getAntiName() : null);
        setTitle(getString(R.string.view_title, (asAnti ? apname : pname)));

        String qs = Integer.toString(vQ / 3);
        if (vQ < 3 && vQ > -3 && vQ != 0) qs = Integer.toString(vQ) + "/3";

        name.setText(asAnti ? apname : pname);
        code.setText(Integer.toString(mCode));
        search_name.setText(vName);
        type.setText(vType);
        charge.setText(qs);
        mass.setText(Html.fromHtml(formatDouble(vMass, 6)));
        width.setText(Html.fromHtml(formatDouble(vWidth, 6)));
        Double lt = vWidth <= 1e-10 ? 1e20 : HBAR / vWidth;
        life.setText(lt >= 1e20 ? "stable" : Html.fromHtml(formatDouble(lt, 4)));

        if (mHasAnti) {
            anti.setText(asAnti ? pname : apname);
            anti.setOnClickListener(new OnClickListener() {
                public void onClick(View v)
                {
                    showAntiParticle();
                }
            });
        }
    }

    /**
     * Format a double precision number using, if necessary scientific notation.
     * 
     * @param v
     *            Value
     * @param max
     *            Max size in chars
     * 
     * @return formatted string
     */
    protected String formatDouble(double v, int max)
    {
        String b = Double.toString(v);
        int i = b.lastIndexOf('E');
        if (i == -1) {
            i = b.lastIndexOf('e');
            if (i == -1) {
                i = b.indexOf('.');
                if (i == -1) return b;
                int l = Math.min(i + max + 1, b.length());
                return b.substring(0, l);
            }
        }
        String e = b.substring(i + 1);
        int l = Math.min(Math.min(i, max + 2), b.length());
        String m = b.substring(0, l);
        return m + "&times;10<sup>" + e + "</sup>";
    }

    /**
     * Query and fill in the decay table.
     * 
     * @param asAnti
     *            If true, then we should 'flip' the product particles
     *            to their anti-particles.
     */
    protected void fillDecays(boolean asAnti)
    {

        Cursor c =
                   managedQuery(Uri.withAppendedPath(Provider.DECAY_URI,
                                                     Integer.toString(mAbsCode)),
                                null, null, null, null);
        if (c == null) return;

        if (c.getCount() >= 1 && c.moveToFirst()) {
            Log.d(TAG, "Got " + c.getCount() + " decay modes");
            TableLayout table = (TableLayout) findViewById(R.id.decays);
            do {
                showDecay(table, c, asAnti);
            } while (c.moveToNext());
        }
        this.stopManagingCursor(c);
        if (!c.isClosed()) c.close();
    }

    /**
     * Listener that reacts to clicks on the decay products.
     * 
     * @author cholm
     * 
     */
    private class ClickListener implements OnClickListener
    {
        /**
         * Constructor
         * 
         * @param code
         *            Particle code
         */
        public ClickListener(int code)
        {
            mTarget = code;
        }

        /**
         * React to clicks.
         */
        public void onClick(View v)
        {
            showParticle(mTarget);
        }

        /** Target particle */
        protected int mTarget;
    }

    /**
     * Show a single decay
     * 
     * @param t
     *            Table
     * @param c
     *            Code of mother.
     * @param asAnti
     *            If true, then we should 'flip' the product particles
     *            to their anti-particles.
     */
    protected void showDecay(TableLayout t, Cursor c, boolean asAnti)
    {

        View view = mInflater.inflate(R.layout.decay_entry, t, false);
        TableRow row = (TableRow) view;
        TextView br = (TextView) row.getChildAt(0);
        // TextView ty = (TextView) row.getChildAt(1);

        br.setText(Html.fromHtml(formatDouble(c.getDouble(2) * 100, 5)));
        // ty.setText(Integer.toString(c.getInt(1)));
        int n = c.getInt(3);
        for (int i = 0; i < n; i++) {
            int pc = c.getInt(4 + i);
            String ps = getProductName(pc, asAnti);
            TextView pv = (TextView) row.getChildAt(2 + (2 * i));
            pv.setText(Html.fromHtml(ps));
            pv.setOnClickListener(new ClickListener(pc));
            if (i != 0) {
                TextView pp = (TextView) row.getChildAt(1 + (2 * i));
                pp.setText("+");
            }
        }

        t.addView(row, new TableLayout.LayoutParams());

    }

    /**
     * Get the name product particle
     * 
     * @param code
     *            Particle code
     * @param asAnti
     *            If true, then query the anti-particle table rather
     *            than the particle table for the name of the decay product.
     * @return Name of product particle.
     */
    protected String getProductName(int code, boolean asAnti)
    {

        Uri.Builder b = mUri.buildUpon();
        b.path(Provider.PARTICLE_PART + "/" + Integer.toString(Math.abs(code)));
        Uri u = b.build();
        Cursor c = managedQuery(u, null, null, null, null);

        if (c == null) return ERROR_STRING;

        String s = ERROR_STRING;
        if (c.getCount() >= 1 && c.moveToFirst()) {
            boolean hasAnti = c.getInt(3) != 0;
            // We can exit here, if
            //
            // The particle does not have an anti-particle at all
            // OR
            // We were asked for a particle (code > 0) and we don't want the
            // anti particle
            // OR
            // We were asked for an anti-particle (code < 0) and we want the
            // anti-particle of that
            //
            if (!hasAnti || (!asAnti && code > 0) || (asAnti && code < 0)) {
                s = c.getString(2);
                // c.close();
            }
            else {
                // We get here only if the above fails, that is if
                // (hasAnti && (asAnti || code < 0) && (!asAnti || code > 0))
                //
                // The particle found does have an anti-particle
                // AND
                // We asked for an anti-particle (code < 0) or we want the
                // anti-particle
                // AND
                // We asked for a particle (code > 0) or we want the
                // anti-particle
                s = getParticleName(-Math.abs(code));
            }
        }
        this.stopManagingCursor(c);
        if (!c.isClosed()) c.close();
        return s;

    }

    /**
     * Get the name of a particle.
     * 
     * @param code
     *            Particle code
     * @return Name of a particle.
     */
    protected String getParticleName(int code)
    {
        Uri.Builder b = mUri.buildUpon();
        b.path((code < 0 ? Provider.ANTIPARTICLE_PART : Provider.PARTICLE_PART)
               + "/" + Integer.toString(Math.abs(code)));
        Uri u = b.build();
        Cursor c = managedQuery(u, null, null, null, null);
        if (c == null) return ERROR_STRING;

        String s = ERROR_STRING;

        if (c.getCount() >= 1 && c.moveToFirst()) s = c.getString(2);

        this.stopManagingCursor(c);
        if (!c.isClosed()) c.close();

        return s;

    }

    /**
     * Re-launch this activity showing another particle
     * 
     * @param code
     *            Particle code
     */
    protected void showParticle(int code)
    {
        if (code == mCode) return;
        String arg = Integer.toString(code);
        Intent particleIntent = new Intent(this, ParticleView.class);
        particleIntent.setData(Uri.withAppendedPath(Provider.PARTICLE_URI, arg));
        startActivity(particleIntent);
        finish();
    }

    /**
     * Re-launch this activity showing the anti-particle
     */
    protected void showAntiParticle()
    {
        showParticle(-mCode);
    }

    /**
     * Get the name of the anti particle
     * 
     * @return
     */
    protected Spanned getAntiName()
    {
        return Html.fromHtml(getParticleName(-mAbsCode));
    }

    /** The intent URI */
    protected Uri            mUri      = null;
    /** Particle code */
    protected int            mCode     = 0;
    /** Absolute value of the particle code */
    protected int            mAbsCode  = 0;
    /** Whether we have an anti particle or not */
    protected boolean        mHasAnti  = false;
    /** XML infalter */
    protected LayoutInflater mInflater = null;
}
/*
 * EOF
 */