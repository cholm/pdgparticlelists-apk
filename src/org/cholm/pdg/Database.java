/*
 * Android application to show particle properties
 * Copyright © 2010 Christian Holm Christensen <cholm@nbi.dk>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 
 */

package org.cholm.pdg;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.StringTokenizer;

import android.app.SearchManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.provider.BaseColumns;
import android.util.Log;

/**
 * @author cholm
 * 
 *         Interface to the Particle listings database.
 */
public class Database
{
    /** A tag */
    private static final String TAG              = Provider.BASE_TAG
                                                   + ".Database";
    /** Database name */
    private static final String DB_NAME          = "Particles";
    /** Database version */
    private static final int    DB_VERSION       = 2;
    /** Maximum number of decay products */
    private static final int    MAX_PRODUCTS     = 10;
    private static final String POPULATED_COLUMN = "populated";

    /**
     * Class to set-up the database
     * 
     * @author cholm
     * 
     */
    private static class DatabaseOpenHelper extends SQLiteOpenHelper
    {
        /** Tag used in debug messages */
        public static final String TAG           = Database.TAG + ".Helper";
        /** Maximum number of particles to read - for debugging */
        private static final int   MAX_PARTICLES = Integer.MAX_VALUE;

        /**
         * Constructor
         * 
         * @param cx
         *            Application context
         */
        public DatabaseOpenHelper(Context cx)
        {
            super(cx, DB_NAME, null, DB_VERSION);
            Log.d(TAG, "Creating database helper object handle=" + mDatabase);
            mContext = cx;
        }

        /**
         * Called when the database should be made
         */
        @Override
        public void onCreate(SQLiteDatabase db)
        {
            Log.d(TAG, "onCreate called for helper w/db=" + db);
            mDatabase = db;
            try {
                Log.d(TAG, "Creating table " + Contract.META_TABLE);
                mDatabase.execSQL("CREATE TABLE " + Contract.META_TABLE + " ("
                                  + POPULATED_COLUMN + ")");
                ContentValues meta = new ContentValues();
                meta.put(POPULATED_COLUMN, 0);
                mDatabase.insert(Contract.META_TABLE, null, meta);

                Log.d(TAG, "Creating table " + Contract.PARTICLES_TABLE);
                String createParticles =
                                         ("CREATE TABLE "
                                          + Contract.PARTICLES_TABLE + " ("
                                          + Contract.CODE_COLUMN + ","
                                          + Contract.NAME_COLUMN + ","
                                          + Contract.PRETTY_COLUMN + ","
                                          + Contract.ANTI_COLUMN + ","
                                          + Contract.TYPE_COLUMN + ","
                                          + Contract.CHARGE_COLUMN + ","
                                          + Contract.MASS_COLUMN + ","
                                          + Contract.WIDTH_COLUMN + ","
                                          + Contract.ISO_COLUMN + ","
                                          + Contract.ISO3_COLUMN + ","
                                          + Contract.SPIN_COLUMN + ");");
                Log.d(TAG, "Executing " + createParticles);
                mDatabase.execSQL(createParticles);

                Log.d(TAG, "Creating table " + Contract.ANTIPARTICLES_TABLE);
                String createAnti =
                                    ("CREATE TABLE "
                                     + Contract.ANTIPARTICLES_TABLE + " ("
                                     + Contract.CODE_COLUMN + ","
                                     + Contract.NAME_COLUMN + ","
                                     + Contract.PRETTY_COLUMN + ");");
                Log.d(TAG, "Executing " + createAnti);
                mDatabase.execSQL(createAnti);

                Log.d(TAG, "Creating table " + Contract.DECAYS_TABLE);
                String createDecays =
                                      ("CREATE TABLE " + Contract.DECAYS_TABLE
                                       + " (" + Contract.MOTHER_COLUMN + ","
                                       + Contract.TYPE_COLUMN + ","
                                       + Contract.RATIO_COLUMN + "," + Contract.NPROD_COLUMN);
                for (int i = 0; i < MAX_PRODUCTS; i++)
                    createDecays += "," + Contract.DAUGHTER_COLUMN + i;
                createDecays += ");";
                Log.d(TAG, "Executing " + createDecays);
                mDatabase.execSQL(createDecays);
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        }

        /**
         * Called when the application is upgraded
         */
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                       + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS " + Contract.PARTICLES_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + Contract.ANTIPARTICLES_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + Contract.DECAYS_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + Contract.META_TABLE);
            onCreate(db);
        }

        /**
         * Starts a thread to load the database tables
         */
        public void loadData()
        {
            Log.d(TAG, "Loading data");
            SQLiteDatabase db = getReadableDatabase();
            if (mDatabase == null) {
                Log.w(TAG, "Setting handle to " + db + " from readable DB");
                mDatabase = db;
            }
            new Thread(new Runnable() {
                public void run()
                {
                    if (!load()) throw new RuntimeException(
                                                            "Failed to load data");
                }
            }).start();
        }

        /**
         * Read the particle listings from the input
         * 
         * @param in
         *            Input stream
         * @return true on success
         */
        public boolean load()
        {
            Log.d(TAG, "Loading database from handle=" + mDatabase);
            try {
                if (mDatabase == null) Log
                                .e(TAG, "Do database handle - will throw next");
                mDatabase.beginTransaction();

                Resources rs = mContext.getResources();
                InputStream in = rs.openRawResource(R.raw.pdg_table);
                mReader =
                          new BufferedReader(new InputStreamReader(in),
                                             8 * 1024);
                Log.d(TAG, "Reading in stuff from file " + R.raw.pdg_table);
                while (mReader != null) {
                    readParticle();
                    if (mCount >= MAX_PARTICLES) break;
                }
                mDatabase.setTransactionSuccessful();
            }
            catch (Exception e) {
                e.printStackTrace();
                return false;
            }
            finally {
                mDatabase.endTransaction();
            }
            ContentValues meta = new ContentValues();
            meta.put(POPULATED_COLUMN, 1);
            mDatabase.update(Contract.META_TABLE, meta, null, null);
            return true;
        }

        /**
         * Read a particle and push it onto our list of particles
         * 
         * @throws Exception
         *             Thrown in case of problems
         */
        protected void readParticle() throws Exception
        {
            if (mParticleRow == null) mParticleRow = new ContentValues();
            if (mAntiParticleRow == null) mAntiParticleRow =
                                                             new ContentValues();
            readInt("index");
            String name = readString(Contract.NAME_COLUMN);
            int code = readInt(Contract.CODE_COLUMN);
            int anti = readInt(Contract.ANTI_COLUMN);
            boolean store = true;

            if (!filterParticle(code, name)) store = false;

            String pretty = beautifyName(code, name);

            Log.d(TAG, "Read particle " + name + "(" + code + ")");
            if (code < 0) {
                mAntiParticleRow.put(Contract.NAME_COLUMN, name);
                mAntiParticleRow.put(Contract.PRETTY_COLUMN, pretty);
                mAntiParticleRow.put(Contract.CODE_COLUMN, code);

                mDatabase.insert(Contract.ANTIPARTICLES_TABLE, null,
                                 mAntiParticleRow);
                readLine();
                return;
            }

            mParticleRow.put(Contract.NAME_COLUMN, name);
            mParticleRow.put(Contract.PRETTY_COLUMN, pretty);
            mParticleRow.put(Contract.CODE_COLUMN, code);
            mParticleRow.put(Contract.ANTI_COLUMN, anti != 0);
            /* int classNum = */readInt("class number");
            mParticleRow.put(Contract.TYPE_COLUMN, readString("class name"));
            mParticleRow.put(Contract.CHARGE_COLUMN, readInt("electric charge"));
            mParticleRow.put(Contract.MASS_COLUMN,
                             readDouble(Contract.MASS_COLUMN));
            mParticleRow.put(Contract.WIDTH_COLUMN,
                             readDouble(Contract.WIDTH_COLUMN));
            mParticleRow.put(Contract.ISO_COLUMN, readInt("iso-spin"));
            mParticleRow.put(Contract.ISO3_COLUMN, readInt("iso-spin-3"));
            mParticleRow.put(Contract.SPIN_COLUMN,
                             readInt(Contract.SPIN_COLUMN));
            readInt("flavour");
            readInt("tracking code");
            int nDecays = readInt("# of decays");

            if (store) {
                mDatabase.insert(Contract.PARTICLES_TABLE, null, mParticleRow);
                mCount++;
            }

            readLine();

            if (nDecays <= 0) return;

            for (int i = 0; i < nDecays; i++)
                readDecay(code);
        }

        /**
         * Filter out particles we don't want in the database
         * 
         * @param code
         *            Particle code
         * @param name
         *            Name
         * @return true if the particle should be kept.
         */
        protected boolean filterParticle(int code, String name)
        {
            switch (code) {
            case 0: // Rootino
            case 81: // specflav
            case 82: // rndmflav
            case -82: // rndmflav_bar
            case 83: // phasespa
            case 91: // cluster
            case 92: // string
            case 93: // indep.
            case 94: // CMshower
            case 95: // SPHEaxis
            case 96: // THRUaxis
            case 97: // CLUSjet
            case 98: // CELLjet
            case 99: // table
                return false;
            }
            return true;
        }

        /**
         * Parses and beautifies the particle name.
         * 
         * @param code
         *            Particle code
         * @param name
         *            Particle name
         * @return Beautified particle name
         */
        protected String beautifyName(int code, String name)
        {
            String ret = "";
            StringBuffer buf = new StringBuffer(name);
            boolean anti = false;
            String charge = "";
            String leftRight = "";
            String sub = "";
            String prime = "";
            String star = "";
            String base = "";
            String tilde = "";
            int len = buf.length();
            final int CHARGE = 2;
            final int LEFTRIGHT = 3;
            final int SUB = 4;
            final int PRIME = 5;
            final int STAR = 6;
            final int BASE = 7;
            final int TILDE = 8;
            final int END = 9;
            int st = CHARGE;

            int idx = buf.lastIndexOf("_bar");
            if (idx >= 0) {
                anti = true;
                buf.delete(idx, len);
                len = buf.length();
            }
            idx = buf.indexOf("anti");
            if (idx == 0) {
                anti = true;
                buf.delete(idx, 4);
                len = buf.length();
            }
            boolean hasSub = buf.indexOf("_") >= 0;
            for (int i = len - 1; i >= 0; i--) {
                char c = buf.charAt(i);
                switch (st) {
                case CHARGE:
                    if (c == '+' || c == '-' || c == '0') {
                        charge = c + charge;
                        break;
                    }
                    st = LEFTRIGHT;
                case LEFTRIGHT:
                    if (c == 'L' || c == 'R') {
                        leftRight = c + leftRight;
                        break;
                    }
                    st = hasSub ? SUB : PRIME;
                case SUB:
                    if (hasSub) {
                        if (c != '_') {
                            sub = c + sub.toString();
                            break;
                        }
                        i--;
                        c = buf.charAt(i);
                    }
                    st = PRIME;
                case PRIME:
                    if (c == '\'' || c == '"') {
                        prime = c + prime;
                        break;
                    }
                    st = STAR;
                case STAR:
                    if (c == '*') {
                        star = c + star;
                        break;
                    }
                    st = BASE;
                case BASE:
                    if (c != '~') {
                        base = c + base;
                        break;
                    }
                case TILDE:
                    if (c == '~') {
                        tilde = c + tilde;
                        break;
                    }
                    st = END;
                case END:
                    break;
                }
            }
            if (sub.length() != 0
                && (sub.charAt(0) == 'R' || sub.charAt(0) == 'L')) {
                leftRight = sub.charAt(0) + leftRight;
                sub = sub.substring(1);
            }

            if (anti && charge.length() == 0) ret +=
                                                     mContext.getString(R.string.anti_start_tag);
            if (tilde.length() != 0) ret +=
                                            mContext.getString(R.string.sparticle_start_tag);
            ret += beautifyBase(base);
            if (tilde.length() != 0) ret +=
                                            mContext.getString(R.string.sparticle_end_tag);
            if (anti && charge.length() == 0) ret +=
                                                     mContext.getString(R.string.anti_end_tag);
            ;
            if (sub.length() != 0 || leftRight.length() != 0) {
                ret += "<sub>";
                if (sub.length() != 0) ret += beautifyBase(sub);
                if (leftRight.length() != 0) ret +=
                                                    (sub.length() == 0 ? ""
                                                                      : " ")
                                                                    + leftRight;
                ret += "</sub>";
            }
            ret += prime;
            if (star.length() != 0 || charge.length() != 0) {
                ret += "<sup>";
                if (star.length() != 0) ret += star;
                if (charge.length() != 0) ret += charge;
                ret += "</sup>";
            }

            return ret;
        }

        /**
         * Beautify the base of a particle name. This returns a UTF-8 encoded
         * string.
         * 
         * @param base
         *            String to beautify
         * @return UTF-8 encoded string.
         */
        protected String beautifyBase(String base)
        {
            String ret = base;
            if (base.compareTo("Lambda") == 0) ret = "\u039b";
            else if (base.compareTo("Upsilon") == 0) ret = "\u03a5";
            else if (base.compareTo("Omega") == 0) ret = "\u03a9";
            else if (base.compareTo("Xi") == 0) ret = "\u039e";
            else if (base.compareTo("Sigma") == 0) ret = "\u03a3";
            else if (base.compareTo("Delta") == 0) ret = "\u0394";
            else if (base.compareTo("mu") == 0) ret = "\u03bc";
            else if (base.compareTo("pi") == 0) ret = "\u03c0";
            else if (base.compareTo("tau") == 0) ret = "\u03c4";
            else if (base.compareTo("nu") == 0) ret = "\u03c5";
            else if (base.compareTo("chi") == 0) ret = "\u03c7";
            else if (base.compareTo("psi") == 0) ret = "\u03c8";
            else if (base.compareTo("phi") == 0) ret = "\u03c6";
            else if (base.compareTo("eta") == 0) ret = "\u03b7";
            else if (base.compareTo("rho") == 0) ret = "\u03c1";
            else if (base.compareTo("omega") == 0) ret = "\u03c9";
            else if (base.compareTo("gamma") == 0) ret = "\u03b3";
            else if (base.compareTo("J/psi") == 0) ret = "J/\u03c8";
            else if (base.compareTo("proton") == 0) ret = "p";
            else if (base.compareTo("neutron") == 0) ret = "n";
            return ret;
        }

        /**
         * Read a decay
         * 
         * @param p
         *            Particle to add the decay channel to
         * @throws Exception
         *             Thrown on read errors
         */
        protected void readDecay(int code) throws Exception
        {
            if (mDecayRow == null) mDecayRow = new ContentValues();

            mDecayRow.put(Contract.MOTHER_COLUMN, code);
            readInt("decay index");
            mDecayRow.put(Contract.TYPE_COLUMN, readInt("decay type"));
            mDecayRow.put(Contract.RATIO_COLUMN, readDouble("branching ratio"));
            int nDaughters = readInt("# of daughters");
            if (nDaughters > MAX_PRODUCTS) nDaughters = MAX_PRODUCTS;

            mDecayRow.put(Contract.NPROD_COLUMN, nDaughters);

            int i = 0;
            for (; i < nDaughters; i++)
                mDecayRow.put(Contract.DAUGHTER_COLUMN + i,
                              readInt("decay product # " + (i + 1)));

            readLine();
            for (; i < MAX_PRODUCTS; i++)
                mDecayRow.putNull(Contract.DAUGHTER_COLUMN + i);

            mDatabase.insert(Contract.DECAYS_TABLE, null, mDecayRow);
        }

        /**
         * Read an integer from the input.
         * 
         * @param what
         *            What we're reading (for the exception handling)
         * @return Read integer
         * @throws Exception
         *             Thrown if no integer is available on the input.
         */
        protected int readInt(String what) throws Exception
        {
            String next = readString(what);
            if (next == null) throw new Exception("Missing " + what);
            return Integer.valueOf(next);
        }

        /**
         * Read a double precision real number from the input.
         * 
         * @param what
         *            What we're reading (for the exception handling)
         * @return Read double precision real number
         * @throws Exception
         *             Thrown if no double precision real number is available on
         *             the input.
         */
        protected double readDouble(String what) throws Exception
        {
            String next = readString(what);
            if (next == null) throw new Exception("Missing " + what);
            return Double.valueOf(next);
        }

        /**
         * Read a string from the input.
         * 
         * @param what
         *            What we're reading (for the exception handling)
         * @return Read string
         * @throws Exception
         *             Thrown if no string is available on the input.
         */
        protected String readString(String what) throws Exception
        {
            if (!checkReader()) return null;
            return mTokens.nextToken();
        }

        protected boolean checkReader() throws Exception
        {
            if (mReader == null) throw new Exception("No scanner or reader");
            if (mTokens == null && !readLine()) return false;
            if (!mTokens.hasMoreTokens() && !readLine()) return false;
            return true;
        }

        protected boolean readLine() throws Exception
        {
            if (mReader == null) throw new Exception("No scanner or reader");
            mTokens = null;
            String line = null;
            do {
                line = mReader.readLine();
                if (line == null) {
                    mReader.close();
                    mReader = null;
                    return false;
                }
                if (line.charAt(0) == '#') continue;
                break;
            } while (true);
            mTokens = new StringTokenizer(line);
            return true;
        }

        /** Context */
        private final Context   mContext;
        /** Database handle */
        private SQLiteDatabase  mDatabase;
        /** Content values for particle rows */
        private ContentValues   mParticleRow     = null;
        /** Content values for particle rows */
        private ContentValues   mAntiParticleRow = null;
        /** Content values for decay rows */
        private ContentValues   mDecayRow        = null;
        /** Particle count */
        private int             mCount           = 0;
        // We would like to have used a java.util.Scanner object instead, but
        // it allocates strings constantly, and that's a killer since the
        // Android garbage collector constantly have to clean up these small
        // objects - causing the file read to take minutes (at least 5) instead
        // of
        // seconds. So we code a 'scanner' ourselves using a buffered reader and
        // a string tokenizer to get the elements.
        /** Tokenizer */
        private StringTokenizer mTokens          = null;
        /** Reader */
        private BufferedReader  mReader          = null;
    }

    /**
     * Constructor
     */
    public Database(Context cx)
    {
        Log.d(TAG, "Creating database object");
        mOpenHelper = new DatabaseOpenHelper(cx);
    }

    /**
     * Builds a map for all columns that may be requested, which will be given
     * to the
     * SQLiteQueryBuilder. This is a good way to define aliases for column
     * names, but must include
     * all columns, even if the value is the key. This allows the
     * ContentProvider to request
     * columns w/o the need to know real column names and create the alias
     * itself.
     */
    private static HashMap<String, String> buildColumnMap()
    {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Contract.NAME_COLUMN, Contract.NAME_COLUMN);
        map.put(Contract.CODE_COLUMN, Contract.CODE_COLUMN);
        map.put(Contract.PRETTY_COLUMN, Contract.PRETTY_COLUMN);
        map.put(Contract.ANTI_COLUMN, Contract.ANTI_COLUMN);
        map.put(Contract.TYPE_COLUMN, Contract.TYPE_COLUMN);
        map.put(Contract.CHARGE_COLUMN, Contract.CHARGE_COLUMN);
        map.put(BaseColumns._ID, Contract.CODE_COLUMN + " AS "
                                 + BaseColumns._ID);
        map.put(SearchManager.SUGGEST_COLUMN_TEXT_1,
                Contract.NAME_COLUMN + " AS "
                                + SearchManager.SUGGEST_COLUMN_TEXT_1); // Search
                                                                        // by
                                                                        // name
        map.put(SearchManager.SUGGEST_COLUMN_TEXT_2,
                Contract.CODE_COLUMN + " AS "
                                + SearchManager.SUGGEST_COLUMN_TEXT_2);
        map.put(SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID,
                Contract.CODE_COLUMN + " AS "
                                + SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID);
        return map;
    }

    /**
     * Returns a Cursor positioned at the particle specified by name
     * 
     * @param name
     *            Name of particle to retrieve
     * @param columns
     *            The columns to include, if null then all are included
     * @return Cursor positioned to matching word, or null if not found.
     */
    public Cursor getParticle(String name, String[] columns)
    {
        String selection = "name = ?";
        String[] selectionArgs = new String[] { name };
        return query(Contract.PARTICLES_TABLE, selection, selectionArgs,
                     columns);
    }

    /**
     * Returns a Cursor positioned at the particle specified by code
     * 
     * @param code
     *            PDG code of the particle to retrieve
     * @param columns
     *            The columns to include, if null then all are included
     * @return Cursor positioned to matching particle, or null if not found.
     */
    public Cursor getParticle(int code, String[] columns)
    {
        String selection = "code=" + code;
        String[] selectionArgs = null; // new String[] {Integer.toString(code)};
        String table =
                       code < 0 ? Contract.ANTIPARTICLES_TABLE
                               : Contract.PARTICLES_TABLE;
        return query(table, selection, selectionArgs, columns);
    }

    /**
     * Returns a Cursor positioned at the particle specified by code
     * 
     * @param code
     *            PDG code of the particle to retrieve the anti-particle for
     * @param columns
     *            The columns to include, if null then all are included
     * @return Cursor positioned to matching particle, or null if not found.
     */
    public Cursor getAntiParticle(int code, String[] columns)
    {
        String selection = "code=" + (-code);
        String[] selectionArgs = null; // new String[]
                                       // {Integer.toString(-code)};
        return query(Contract.ANTIPARTICLES_TABLE, selection, selectionArgs,
                     columns);
    }

    /**
     * Get a cursor over all decays for a single particle
     * 
     * @param mother
     *            PDG code of the particle to retrieve decays for
     * @param columns
     *            The columns to include, if null then all are included
     * @return Cursor positioned to matching decays, or null if none found.
     */
    public Cursor getDecays(int mother, String[] columns)
    {
        String selection = Contract.MOTHER_COLUMN + "=" + mother;
        String[] selectionArgs = null; // new String[]
                                       // {Integer.toString(mother)};
        return query(Contract.DECAYS_TABLE, selection, selectionArgs, columns);
    }

    /**
     * Returns a Cursor over all words that match the given query
     * 
     * @param query
     *            The string to search for
     * @param columns
     *            The columns to include, if null then all are included
     * @return Cursor over all words that match, or null if none found.
     */
    public Cursor getParticleMatches(String selection,
                                     String[] selectionArgs,
                                     String[] columns)
    {
        return this.queryWithAlias(Contract.PARTICLES_TABLE, selection,
                                   selectionArgs, columns);
    }

    /**
     * Check if the database has been populated
     * 
     * @return true if the database has been filled from the data file
     */
    public Cursor dbExists()
    {
        String[] cols = { POPULATED_COLUMN };
        Cursor ret = query(Contract.META_TABLE, null, null, cols);
        ret.moveToFirst();
        return ret;
    }

    /**
     * Load data into the database. This forwards the request to the
     * helper class
     * 
     * @return A cursor or null.
     */
    public Cursor loadData()
    {
        this.mOpenHelper.loadData();
        String[] cols = { "ok" };
        MatrixCursor ret = new MatrixCursor(cols);
        MatrixCursor.RowBuilder rb = ret.newRow();
        rb.add(1);
        return ret;
    }

    /**
     * Execute a query on the database
     * 
     * @param table
     *            Table to query
     * @param selection
     *            Selection (or null for all)
     * @param selectionArgs
     *            Selection arguments
     * @param columns
     *            Columns to get
     * @return A cursor on the result set or null
     */
    public Cursor query(String table,
                        String selection,
                        String[] selectionArgs,
                        String[] columns)
    {
        Cursor cursor = null;
        try {
            cursor =
                     mOpenHelper.getReadableDatabase().query(table, columns,
                                                             selection,
                                                             selectionArgs,
                                                             null, null, null);
        }
        catch (Exception e) {
            e.printStackTrace();
            cursor = null;
        }
        return cursor;
    }

    /**
     * Performs a database query with aliases.
     * 
     * @param table
     *            Table name
     * @param selection
     *            The selection clause
     * @param selectionArgs
     *            Selection arguments for "?" components in the selection
     * @param columns
     *            The columns to return
     * @return A Cursor over all rows matching the query
     */
    public Cursor queryWithAlias(String table,
                                 String selection,
                                 String[] selectionArgs,
                                 String[] columns)
    {
        /*
         * The SQLiteBuilder provides a map for all possible columns requested
         * to
         * actual columns in the database, creating a simple column alias
         * mechanism
         * by which the ContentProvider does not need to know the real column
         * names
         */
        Cursor cursor = null;
        try {
            SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
            builder.setTables(table);
            if (table.compareTo(Contract.PARTICLES_TABLE) == 0) builder
                            .setProjectionMap(mColumnMap);

            cursor =
                     builder.query(mOpenHelper.getReadableDatabase(), columns,
                                   selection, selectionArgs, null, null, null);

            if (cursor == null) return null;
            else if (!cursor.moveToFirst()) {
                cursor.close();
                return null;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return cursor;
    }

    /** Opener */
    protected final DatabaseOpenHelper           mOpenHelper;
    /** Alias map */
    private static final HashMap<String, String> mColumnMap = buildColumnMap();
}
/*
 * EOF
 */
