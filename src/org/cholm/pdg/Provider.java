/*
 * Android application to show particle properties
 * Copyright © 2010 Christian Holm Christensen <cholm@nbi.dk>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 
 */
package org.cholm.pdg;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

/**
 * @author cholm
 * 
 *         Provides access to the particle listing data stored in the database
 */
public class Provider extends ContentProvider
{
    public static String       BASE_TAG            = "org.cholm.pdg";
    public static String       TAG                 = BASE_TAG + ".Provider";

    /** Provider ID */
    public static String       AUTHORITY           = "org.cholm.pdg.Provider";
    public static String       PARTICLE_PART       = "particles";
    public static String       ANTIPARTICLE_PART   = "anti" + PARTICLE_PART;
    public static String       DECAY_PART          = "decays";
    public static String       DB_PART             = "db";
    /** Contents URI */
    public static final Uri    PARTICLES_URI       = Uri.parse("content://"
                                                               + AUTHORITY
                                                               + "/"
                                                               + PARTICLE_PART);
    public static final Uri    PARTICLE_URI        = Uri.parse("content://"
                                                               + AUTHORITY
                                                               + "/"
                                                               + PARTICLE_PART);
    public static final Uri    DECAY_URI           = Uri.parse("content://"
                                                               + AUTHORITY
                                                               + "/"
                                                               + DECAY_PART);
    public static final Uri    DB_URI              = Uri.parse("content://"
                                                               + AUTHORITY
                                                               + "/" + DB_PART);

    // MIME types used for searching words or looking up a single definition
    public static final String PARTICLES_MIME_TYPE =
                                                     ContentResolver.CURSOR_DIR_BASE_TYPE
                                                                     + "/vnd.cholm.pdg.particles";
    public static final String DECAYS_MIME_TYPE    =
                                                     ContentResolver.CURSOR_DIR_BASE_TYPE
                                                                     + "/vnd.cholm.pdg.decays";
    public static final String PARTICLE_MIME_TYPE  =
                                                     ContentResolver.CURSOR_ITEM_BASE_TYPE
                                                                     + "/vnd.cholm.pdg.particle";
    public static final String DB_MIME_TYPE        =
                                                     ContentResolver.CURSOR_ITEM_BASE_TYPE
                                                                     + "/vnd.cholm.pdg.db";
    public static final int    SEARCH_PARTICLES    = 1;
    public static final int    GET_PARTICLE        = 2;
    public static final int    GET_ANTIPARTICLE    = 3;
    public static final int    SEARCH_SUGGEST      = 4;
    public static final int    GET_DECAYS          = 5;
    public static final int    DB_EXISTS           = 6;

    /**
     * Builds up a UriMatcher for search suggestion and shortcut refresh
     * queries.
     */
    private static UriMatcher buildUriMatcher()
    {
        UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        // to get definitions...
        matcher.addURI(AUTHORITY, PARTICLE_PART + "/#", GET_PARTICLE);
        matcher.addURI(AUTHORITY, ANTIPARTICLE_PART + "/#", GET_ANTIPARTICLE);
        matcher.addURI(AUTHORITY, PARTICLE_PART, SEARCH_PARTICLES);
        matcher.addURI(AUTHORITY, DECAY_PART + "/#", GET_DECAYS);
        matcher.addURI(AUTHORITY, DB_PART, DB_EXISTS);
        // to get suggestions...
        matcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_QUERY,
                       SEARCH_SUGGEST);
        matcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_QUERY + "/*",
                       SEARCH_SUGGEST);

        return matcher;
    }

    /*
     * (non-Javadoc)
     * 
     * @see android.content.ContentProvider#getType(android.net.Uri)
     */
    @Override
    public String getType(Uri uri)
    {
        switch (mURIMatcher.match(uri)) {
        case SEARCH_PARTICLES:
            return PARTICLES_MIME_TYPE;
        case GET_PARTICLE:
            return PARTICLE_MIME_TYPE;
        case GET_ANTIPARTICLE:
            return PARTICLE_MIME_TYPE;
        case SEARCH_SUGGEST:
            return SearchManager.SUGGEST_MIME_TYPE;
        case GET_DECAYS:
            return DECAYS_MIME_TYPE;
        case DB_EXISTS:
            return DB_MIME_TYPE;
        default:
            throw new IllegalArgumentException("Unknown URL " + uri);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see android.content.ContentProvider#onCreate()
     */
    @Override
    public boolean onCreate()
    {
        Log.d(TAG, "Creating provider");
        mDatabase = new Database(getContext());

        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see android.content.ContentProvider#query(android.net.Uri,
     * java.lang.String[], java.lang.String, java.lang.String[],
     * java.lang.String)
     */
    @Override
    public Cursor query(Uri uri,
                        String[] projection,
                        String selection,
                        String[] selectionArgs,
                        String sortOrder)
    {
        // Use the UriMatcher to see what kind of query we have and format the
        // db query accordingly
        Log.i(TAG,
              "Query with URI='" + uri + "' match=" + mURIMatcher.match(uri)
                              + " selection=" + selection + " args="
                              + selectionArgs);
        switch (mURIMatcher.match(uri)) {
        case SEARCH_SUGGEST:
            if (selectionArgs == null) { throw new IllegalArgumentException(
                                                                            "selectionArgs must be provided for the Uri: "
                                                                                            + uri); }
            return getSuggestions(selectionArgs[0]);
        case SEARCH_PARTICLES:
            return mDatabase.getParticleMatches(selection, selectionArgs,
                                                projection);
        case GET_PARTICLE:
            return getParticle(uri);
        case GET_ANTIPARTICLE:
            return getAntiParticle(uri);
        case GET_DECAYS:
            return getDecays(uri);
        case DB_EXISTS:
            if (selection != null && selection.compareTo("exists") == 0) return dbExists();
            return populateDb();
        default:
            throw new IllegalArgumentException("Unknown Uri: " + uri);
        }
    }

    private boolean isInteger(String input)
    {
        try {
            Integer.parseInt(input);
            return true;
        }
        catch (Exception e) {}
        return false;
    }

    /**
     * Get search suggestions
     * 
     * @param query
     *            Particles to search for
     * @return Cursor
     */
    private Cursor getSuggestions(String query)
    {
        String[] columns =
                           new String[] {
                                           BaseColumns._ID,
                                           SearchManager.SUGGEST_COLUMN_TEXT_1,
                                           SearchManager.SUGGEST_COLUMN_TEXT_2,
                                           SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID };

        String[] args = new String[] { query + "%" };
        String col =
                     isInteger(query) ? Contract.CODE_COLUMN
                                     : Contract.NAME_COLUMN;
        return mDatabase.queryWithAlias(Contract.PARTICLES_TABLE, col
                                                                  + " LIKE ?",
                                        args, columns);
    }

    /**
     * Get a single particle
     * 
     * @param uri
     *            URI
     * @return Cursor pointing at particle
     */
    private Cursor getParticle(Uri uri)
    {
        String code = uri.getLastPathSegment();
        Log.d(TAG, "Got request for particle " + code);
        return mDatabase.getParticle(Integer.valueOf(code).intValue(), null);
    }

    /**
     * Get a single particle
     * 
     * @param uri
     *            URI
     * @return Cursor pointing at particle
     */
    private Cursor getAntiParticle(Uri uri)
    {
        String code = uri.getLastPathSegment();
        Log.d(TAG, "Got request for particle -" + (code));
        return mDatabase.getAntiParticle(Integer.valueOf(code).intValue(), null);
    }

    /**
     * Get decays for a single particle
     * 
     * @param uri
     *            URI
     * @return Cursor pointing at decays
     */
    private Cursor getDecays(Uri uri)
    {
        String code = uri.getLastPathSegment();
        return mDatabase.getDecays(Integer.valueOf(code).intValue(), null);
    }

    /**
     * Check if the database is populated
     * 
     * @return Cursor with a single row/column
     */
    private Cursor dbExists()
    {
        return mDatabase.dbExists();
    }

    /**
     * Ask database object to populate the database
     * 
     * @return
     */
    private Cursor populateDb()
    {
        Log.d(TAG, "Populating database");
        return mDatabase.loadData();
    }

    /*
     * (non-Javadoc)
     * 
     * @see android.content.ContentProvider#delete(android.net.Uri,
     * java.lang.String, java.lang.String[])
     */
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     * 
     * @see android.content.ContentProvider#insert(android.net.Uri,
     * android.content.ContentValues)
     */
    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        throw new UnsupportedOperationException();
    }

    /*
     * (non-Javadoc)
     * 
     * @see android.content.ContentProvider#update(android.net.Uri,
     * android.content.ContentValues, java.lang.String, java.lang.String[])
     */
    @Override
    public int update(Uri uri,
                      ContentValues values,
                      String selection,
                      String[] selectionArgs)
    {
        throw new UnsupportedOperationException();
    }

    /** Our database */
    private Database                mDatabase;
    /** URI matcher */
    private static final UriMatcher mURIMatcher = buildUriMatcher();

}
/*
 * EOF
 */
